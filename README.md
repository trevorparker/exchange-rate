README
======

Install
-------

This is a standard Ruby on Rails app using MySQL. Running `rake db:create` and `rake db:migrate` should get you set up. An initial `rake db:populate` will pull down current exchange rates and create the necessary rows.

Updating exchange rates
-----------------------

Updating exchange rates is possible by running `rake db:populate`. This will update existing currencies, and create rows for any new ones.
