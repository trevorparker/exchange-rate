ExchangeRate::Application.routes.draw do
  get 'converter/index'
  post 'converter/convert'

  root 'converter#index'
end
