namespace :db do
  desc 'Fill database with data'
  task populate: :environment do
    require 'rss'
    require 'open-uri'

    puts 'Updating currencies ...'

    url = 'http://themoneyconverter.com/rss-feed/USD/rss.xml'
    open(url) do |rss|
      feed = RSS::Parser.parse(rss)
      feed.items.each do |item|
        begin
        # 1 United States Dollar = 1.10289 Australian Dollar
        #                          <value>     <label>
        /=\s(?<value>[\d,]+\.\d+)\s(?<label>.+)$/ =~ item.description

        abbreviation = item.title.split('/')[0]

        # Store everything as the value, in USD, of 1 unit of this currency
        rate = 1 / value.gsub(',','').to_f

        currency = Currency.find_or_create_by(abbreviation: abbreviation)
        currency.update!(label: label, rate: rate)
        rescue
          $stderr.puts "Unable to update exchange rate: #{item.title}"
        end
      end
    end

    puts 'Done!'
  end
end
