class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :label
      t.string :abbreviation
      t.decimal :rate, precision: 10, scale: 5
      t.datetime :updated_at

      t.timestamps
    end
    add_index :currencies, :abbreviation, unique: true
  end
end
