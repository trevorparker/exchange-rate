class ConverterController < ApplicationController
  def index
    @currencies = Currency.all
  end

  def convert
    starting_currency = Currency.find(params[:from_currency_id])
    ending_currency = Currency.find(params[:to_currency_id])
    amount = params[:amount].to_f

    # Convert our starting currency to USD, then use the ending currency's
    # rate to determine the value.
    usd_value = amount * starting_currency.rate.to_f
    ending_value = usd_value / ending_currency.rate.to_f

    @starting_currency = starting_currency.label
    @ending_currency = ending_currency.label

    # Didn't sprintf or round this, because we took it in as a float and
    # did our math at that precision.
    @amount = amount

    # Round the result and ensure we have a familiar, hundredths-place
    # decimal notation.
    @result = sprintf("%.2f", ending_value.round(2))
  end
end
