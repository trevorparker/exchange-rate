require 'test_helper'

class ConverterControllerTest < ActionController::TestCase
  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'should get convert' do
    post :convert, :from_currency_id => 1, :to_currency_id => 2, :value => 1
    assert_response :success
  end

end
